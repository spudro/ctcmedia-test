<?php

declare(strict_types=1);


use Calculator\SimplyCalculator\SimplyCalculator;
use Calculator\SuccessfulResult;
use PHPUnit\Framework\TestCase;

class SimplyCalculatorTest extends TestCase
{

    /**
     * @covers \Calculator\SimplyCalculator\SimplyCalculator::calculate()
     */
    public function testCalculate(): void
    {
        $sc = new SimplyCalculator();
        $r = $sc->calculate('1 1 +');
        $this->assertInstanceOf(SuccessfulResult::class, $r);
        $this->assertSame('2', $r->getResult());


        $this->assertSame('3', $sc->calculate('1 1 1 + +')->getResult());
        $this->assertSame('4', $sc->calculate('1 1 + 1 * 2 *')->getResult());
        $this->assertSame('13', $sc->calculate('11 1 1 + +')->getResult());
        //4 + 5 = 9
        //9 - 3 = 6
        //6 * 2 = 12
        //12 / 1 = 12
        $this->assertSame('12', $sc->calculate('1 2 3 4 5 + - * /')->getResult());

        $this->assertSame('15', $sc->calculate('1 2 + 4 * 3 +')->getResult());
    }
}
