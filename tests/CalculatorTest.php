<?php

declare(strict_types=1);


use Calculator\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{

    public function test__construct(): void
    {
        $s = new Calculator();
//        $calcMock = $this->createMock(Calculator::class);
        $this->assertSame('2', $s->calculate('1 1 +')->getResult());

        $s = new Calculator(new class implements \Calculator\CalculateInterface
        {

            public function calculate(string $expression): \Calculator\Result
            {
                return new \Calculator\SuccessfulResult($expression);
            }
        });

        $this->assertSame('123', $s->calculate('123')->getResult());

    }
}
