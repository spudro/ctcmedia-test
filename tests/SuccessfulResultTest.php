<?php

declare(strict_types=1);


use Calculator\SuccessfulResult;
use PHPUnit\Framework\TestCase;

class SuccessfulResultTest extends TestCase
{

    /**
     * @covers \Calculator\SuccessfulResult::getResult()
     */
    public function testGetResult(): void
    {

        $sr = new SuccessfulResult('kek');
        $this->assertSame('kek', $sr->getResult());
    }
}
