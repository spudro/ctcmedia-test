<?php

declare(strict_types=1);


use Calculator\FailedResult;
use PHPUnit\Framework\TestCase;

class FailedResultTest extends TestCase
{

    /**
     * @var FailedResult
     */
    protected $failedResult;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $this->failedResult = new FailedResult(['fail1', 'fail2', 'fail3']);
    }

    /**
     * @covers \Calculator\FailedResult::getResult()
     */
    public function testGetResult(): void
    {
        $this->assertSame('fail1|fail2|fail3', $this->failedResult->getResult());
    }

    /**
     * @covers \Calculator\FailedResult::getErrors()
     */
    public function testGetErrors(): void
    {
        $this->assertSame(['fail1', 'fail2', 'fail3'], $this->failedResult->getErrors());
    }
}
