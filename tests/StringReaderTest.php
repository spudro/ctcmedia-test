<?php

declare(strict_types=1);


use Calculator\SimplyCalculator\StringReader;
use PHPUnit\Framework\TestCase;

class StringReaderTest extends TestCase
{

    /**
     * @var StringReader
     */
    protected $stringReader;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $this->stringReader = new StringReader('kek lol');
    }

    /**
     * @covers \Calculator\SimplyCalculator\StringReader::getChar()
     * @covers \Calculator\SimplyCalculator\StringReader::getPosition()
     * @covers \Calculator\SimplyCalculator\StringReader::pushBackChar()
     */
    public function testGetChar(): void
    {
        $this->assertSame(0, $this->stringReader->getPosition());
        $this->assertSame('k', $this->stringReader->getChar());
        $this->assertSame(1, $this->stringReader->getPosition());
        $this->stringReader->pushBackChar();
        $this->assertSame(0, $this->stringReader->getPosition());
        $this->assertSame('k', $this->stringReader->getChar());
    }
}
