FROM php:7.1-cli
WORKDIR /code
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN apt-get update \
  && apt-get install zip unzip \
  && pecl install xdebug \
  && docker-php-ext-enable xdebug
#  \
#  && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
#  && php composer-setup.php \
#  && php -r "unlink('composer-setup.php');" \
#  && php composer.phar install