<?php

declare(strict_types=1);


namespace Calculator;


abstract class Result
{
 abstract public function getResult() : string;
}