<?php

declare(strict_types=1);


namespace Calculator;


use Calculator\SimplyCalculator\SimplyCalculator;

class Calculator implements CalculateInterface
{

    /**
     * @var CalculateInterface
     */
    protected $calculator;

    public function __construct(CalculateInterface $calculateEngine = null)
    {
        if (!$calculateEngine) {
            $this->calculator = new SimplyCalculator();
            return;
        }

        $this->calculator = $calculateEngine;
    }


    /**
     * @codeCoverageIgnore
     * @param string $expression
     * @return Result
     */
    public function calculate(string $expression): Result
    {
        return $this->calculator->calculate($expression);
    }
}