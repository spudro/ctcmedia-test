<?php

declare(strict_types=1);


namespace Calculator;

class SuccessfulResult extends Result
{
    /**
     * @var string
     */
    protected $value;

    /**
     * @codeCoverageIgnore
     * @param string $result
     */
    public function __construct(string $result)
    {
        $this->value = $result;
    }


    public function getResult(): string
    {
        return $this->value;
    }
}