<?php

declare(strict_types=1);


namespace Calculator;


class FailedResult extends Result
{
    protected $errors;

    /**
     * @codeCoverageIgnore
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        $this->errors = $errors;
    }

    public function getResult(): string
    {
        return \implode('|', $this->errors);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}