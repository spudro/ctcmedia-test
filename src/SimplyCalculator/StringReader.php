<?php

declare(strict_types=1);

namespace Calculator\SimplyCalculator;

class StringReader
{
    private $in;
    private $position;

    /**
     * @codeCoverageIgnore
     * StringReader constructor.
     * @param string $in
     */
    public function __construct(string $in)
    {
        $this->in = $in;
        $this->position = 0;
    }

    public function getChar(): string
    {
        if ($this->position >= \strlen($this->in)) {
            return '';
        }
        $char = \mb_substr($this->in, $this->position, 1);
        $this->position++;
        return $char;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function pushBackChar(): void
    {
        $this->position--;
    }
}