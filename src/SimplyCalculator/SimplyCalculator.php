<?php

declare(strict_types=1);


namespace Calculator\SimplyCalculator;


use Calculator\CalculateInterface;
use Calculator\FailedResult;
use Calculator\Result;
use Calculator\SuccessfulResult;

class SimplyCalculator implements CalculateInterface
{

    public function calculate(string $expression): Result
    {
        if ($expression === '') {
            return new FailedResult(['Empty string.']);
        }

        /**
         * @codeCoverageIgnore
         */
        $stringReader = new StringReader($expression);

        $stack = new \SplStack();

        while (($symbol = $stringReader->getChar()) !== '') {

            if (\is_numeric($symbol) && \is_numeric($stringReader->getChar())) {
                $stringReader->pushBackChar();
                while (\is_numeric($nextSymbol = $stringReader->getChar())) {
                    $symbol .= $nextSymbol;
                }
                $stack->push((int)$symbol);
            }

            if (\is_numeric($symbol)) {
                $stack->push((int)$symbol);
            }

            if ($symbol === '+') {
                $stack->push($stack->pop() + $stack->pop());
            }

            if ($symbol === '-') {
                $stack->push($stack->pop() - $stack->pop());
            }

            if ($symbol === '*') {
                $stack->push($stack->pop() * $stack->pop());
            }

            if ($symbol === '/') {
                $stack->push($stack->pop() / $stack->pop());
            }
        }

        return new SuccessfulResult((string)$stack->top());
    }
}