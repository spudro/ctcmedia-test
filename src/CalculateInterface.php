<?php

declare(strict_types=1);


namespace Calculator;


interface CalculateInterface
{
    public function calculate(string $expression): Result;
}