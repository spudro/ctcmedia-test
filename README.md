
Инициализация калькулятора через 
Дефолтный калькулятор работает в обратной польской нотации.
```
 $calculator = new Calculator();
 
 print $calculator->calculate('1 1 +')->getResult();
```

Для использования другого "движка" для вычислений достаточно реализовать интерфейс
`CalculateInterface`.

```
 class NewCalculator implement CalculateInterface{
     public function calculate(string $expression): Result {
           return new Result();
     }
}

$calculator = new Calculator(new NewCalculator());
```